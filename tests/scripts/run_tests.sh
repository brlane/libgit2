#!/bin/bash
set -eux

make

if [ ! -e ~/.gitconfig ]; then
    cat << EOF > ~/.gitconfig
[user]
name=test-user
email=test-email
EOF
fi

./init --initial-commit ./test-libgit2
pushd ./test-libgit2
../log | grep 'Initial commit'
